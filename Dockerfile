FROM alpine:latest

RUN apk --no-cache add ca-certificates alpine-sdk build-base apk-tools alpine-conf busybox fakeroot syslinux xorriso squashfs-tools mtools dosfstools grub-efi

RUN wget https://raw.githubusercontent.com/alpinelinux/alpine-make-vm-image/v0.6.0/alpine-make-vm-image && echo 'a0fc0b4886b541bb4dd7b91b4e1e99719a1700da  alpine-make-vm-image' | sha1sum -c || exit 1
RUN mv ./alpine-make-vm-image /usr/local/bin && chmod 777 /usr/local/bin/alpine-make-vm-image && chmod +X /usr/local/bin/alpine-make-vm-image
